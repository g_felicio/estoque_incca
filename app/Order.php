<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'item_id',
        'quantity',
        'arrival',
        'arrived'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id', 'id');
    }
}
