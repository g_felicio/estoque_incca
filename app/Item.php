<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name',
        'description',
        'unit_price',
        'stock_quantity',
        'stock_price',
        'minimum_stock'
    ];

    public function order_pending()
    {
        return $this->hasMany('App\Order', 'item_id', 'id')->where('arrived', 0);
    }

    public function order_done()
    {
        return $this->hasMany('App\Order', 'item_id', 'id')->where('arrived', 1);
    }
}
