<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $perPage = 5;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderBy('id', 'DESC')->paginate($this->perPage);

        $returns['items'] = $items;

        return view('pages.items.index', $returns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entryData = [];
        $data = $request->all();

        $message = [
            'name.required' => "Campo Nome é obrigatório.",
            'name.max' => "Campo Nome deve conter, no máximo, 100 caracteres.",
            'unit_price.required' => "Campo de Preço é obrigatório.",
            'stock_quantity.required' => "Campo de Quantidade é obrigatório"
        ];

        $this->validate($request, [
            'name' => "required|max:100",
            'description' => "required|max:150",
            'unit_price' => "required",
            'stock_quantity' => "required",
        ], $message);

        $entryData['name'] = $data['name'];
        $entryData['description'] = $data['description'];
        $entryData['unit_price'] = $data['unit_price'];
        $entryData['stock_quantity'] = $data['stock_quantity'];
        $entryData['stock_price'] = $data['stock_quantity'] * $data['unit_price'];
        $entryData['minimum_stock'] = $data['stock_quantity'];

        $item = Item::create($entryData);
        if ($item) {
            return redirect()
                ->route('items.index')
            ->with('message', 'Item criado com sucesso!');
        }

        return redirect()
            ->route('items.create')
        ->with('message', 'Ocorreu um erro ao criar o Item. Verifique as informações e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::where('id', $id)->first();

        if ($item) {
            $returns['item'] = $item;
            return view('pages.items.show', $returns);
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Item não encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::where('id', $id)->first();

        if ($item) {
            $returns['item'] = $item;
            return view('pages.items.edit', $returns);
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Item não pode ser encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = [];
        $data = $request->all();

        $updateData['name'] = $data['name'];
        $updateData['description'] = $data['description'];
        $updateData['unit_price'] = $data['unit_price'];
        $updateData['minimum_stock'] = $data['minimum_stock'];

        $item = Item::where('id', $id)->first();

        if ($item) {
            $quantity = $item->stock_quantity;
            $stockPrice = $updateData['unit_price'] * $quantity;
            $updateData['stock_price'] = $stockPrice;

            $result = $item->fill($updateData)->save();

            if ($result) {
                return redirect()
                    ->route('items.index')
                ->with('message', 'Item atualizado com sucesso!');
            }

            return redirect()
                ->route('items.edit', $id)
            ->with('message', 'Ocorreu um erro ao atualizar o Item. Verifique as informações e tente novamente. Caso o erro persista entre em contato com o suporte');
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Item não encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::where('id', $id)->first();

        if ($item) {
            $expected = $item->delete();

            if ($expected) {
                return redirect()
                    ->route('items.index')
                ->with('message', 'Item deletado com sucesso.');
            }

            return redirect()
                ->route('items.index')
            ->with('message', 'Ocorreu um erro ao deletar o Item. Verifique as informações e tente novamente. Caso o erro persista entre em contato com o suporte.');
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Item não encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }
}
