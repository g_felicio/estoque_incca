<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $returns = [];
        $user = User::where('id', $id)->first();

        if ($user) {
            if ($user->id === Auth::user()->id) {
                $returns['user'] = $user;

                return view('pages.users.edit', $returns);
            }

            return redirect()
                ->route('home')
            ->with('message', 'Ação não permitida.');
        }

        return redirect()
            ->route('home')
        ->with('message', 'Usuário não encontrado. Certifique-se de que este existe e tente novamente. Caso o problema persista entre em contato com o suporte.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::where('id', $id)->first();

        if ($user) {
            if ($user->id === Auth::user()->id) {
                if (isset($data['name']) && isset($data['email'])) {
                    $result = $user->fill($data)->save();

                    if ($result) {
                        return redirect()
                            ->route('users.edit', $user->id)
                        ->with('message', 'Dados editados com sucesso!');
                    }

                    return redirect()
                        ->route('users.edit', $user->id)
                    ->with('message', 'Erro ao atualizar dados. Certifique-se de que os dados estão corretos e tente novamente. Caso o erro persista entre em contato com o suporte.');
                }

                return redirect()
                    ->route('users.edit', $id)
                ->with('message', 'Ação não permitida.');
            }
        }

        return redirect()
            ->route('home')
        ->with('message', 'Usuário não encontrado. Certifique-se de que este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
