<?php

namespace App\Http\Controllers;

use App\Item;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();
        $returns['items'] = $items;

        return 'OK';
    }

    public function orderItem($itemId)
    {
        $item = Item::where('id', $itemId)->first();

        if ($item) {
            $returns['item'] = $item;

            if (count($item->order_pending) > 0) {
                $orderPending = $item->order_pending->sortByDesc('id')->first();
                $order = Order::find($orderPending->id);
                $returns['order_pending'] = $order;
            }

            return view('pages.orders.create', $returns);
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Item não pode ser encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $message = [
            'item_id.required' => "Campo de ID de Item Obrigatório. Entre em contato com o suporte.",
            'arrival.required' => "Campo Previsão de Chegada obrigatório.",
            'quantity.required' => "Campo de Quantidade é obrigatório"
        ];

        $this->validate($request, [
            'item_id' => "required",
            'arrival' => "required",
            'quantity' => "required",
        ], $message);

        $item = Item::where('id', $data['item_id'])->first();
        if ($item) {
            $order = Order::create($data);
            if ($order) {
                return redirect()
                    ->route('items.index')
                ->with('message', 'Pedido para o Item '.$item->id. ' - ' .$item->name. ' feito com sucesso!');
            }

            return redirect()
                ->route('items.index')
            ->with('message', 'Pedido não pode ser concluído. Verifique as informações e tente novamente. Caso o erro persista entre em contato com o suporte.');
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Item não pode ser encontrado. Verifique se este existe e tente refazer o Pedido. Este pedido não foi concluído. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('id', $id)->first();

        if ($order) {
            $returns['order'] = $order;

            return view('pages.orders.show', $returns);
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Pedido não encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::where('id', $id)->first();

        if ($order) {
            $item = Item::where('id', $order->item_id)->first();

            $returns['order'] = $order;
            $returns['item'] = $item;

            return view('pages.orders.edit', $returns);
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Pedido não encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = [];
        $itemData = [];

        $order = Order::where('id', $id)->first();
        if ($order) {
            $data = $request->all();

            $updateData['item_id'] = $data['item_id'];
            $updateData['arrival'] = $data['arrival'];
            $updateData['quantity'] = $data['quantity'];
            if ($data['arrived'] == 'on') {
                $updateData['arrived'] = 1;
            } else {
                $updateData['arrived'] = 0;
            }

            $item = Item::where('id', $data['item_id'])->first();
            if ($item) {
                $qtBought = $data['quantity'];
                $stockQt = $item->stock_quantity;
                $newQt = $qtBought + $stockQt;

                $itemValue = $item->unit_price;
                $newItemValue = $newQt * $itemValue;

                $itemData['stock_quantity'] = $newQt;
                $itemData['stock_price'] = $newItemValue;

                $itemExpect = $item->fill($itemData)->save();
                $orderExpect = $order->fill($updateData)->save();
                if ($itemExpect && $orderExpect) {
                    return redirect()
                        ->route('items.index')
                    ->with('message', 'Pedido finalizado com sucesso. O Item foi atualizado. Cheque as informações.');
                }

                return redirect()
                    ->route('items.index')
                ->with('message', 'Pedido e Item não puderam ser atualizados. Entre em contato com o suporte.');
            }

            return redirect()
                ->route('items.index')
            ->with('message', 'Item não pode ser recuperado. Pedido e Item não puderam ser atualizados. Entre em contato com o suporte.');
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Pedido não pode serrecuperado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
        dd($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::where('id', $id)->first();

        if ($order) {
            $expected = $order->delete();

            if ($expected) {
                return redirect()
                    ->route('items.index')
                ->with('message', 'Pedido cancelado com sucesso!');
            }

            return redirect()
                ->route('items.index')
            ->with('message', 'Ocorreu um erro ao cancelar o pedido. Verifique as informações e tente novamente. Caso o erro persista entre em contato com o suporte.');
        }

        return redirect()
            ->route('items.index')
        ->with('message', 'Pedido não encontrado. Verifique se este existe e tente novamente. Caso o erro persista entre em contato com o suporte.');
    }
}
