<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->catchPhrase,
        'unit_price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 20),
        'stock_quantity' => $faker->randomNumber($nbDigits = 2, $strict = false),
        'minimum_stock' => $faker->randomNumber($nbDigits = 2, $strict = false)
    ];
});
