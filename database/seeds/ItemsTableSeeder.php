<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Item::class, 20)->create();

        $allItems = Item::all();

        foreach ($allItems as $item) {
            $unitPrice = $item->unit_price;
            $stockQt = $item->stock_quantity;

            $stockValue = $unitPrice * $stockQt;

            $item->stock_price = $stockValue;
            $item->save();
        }
    }
}
