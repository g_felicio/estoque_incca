<div class="sidebar">
    <a class="sidebar__link {{ Request::is('users*') ? 'active' : '' }}"
        href="{{ route('users.edit', Auth::user()->id) }}">
        Editar Usuário
    </a>

    <a class="sidebar__link {{ Request::is('items*') ? 'active' : '' }}"
        href="{{ route('items.index') }}">
        Estoque
    </a>
</div>
