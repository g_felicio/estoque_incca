<header class="header">
    <a class="navbar-brand" href="{{ route('home') }}">
        Estoque INCCA
    </a>

    @guest
        @include('components.header_guest_component')
    @else
        @include('components.header_user_component')
    @endguest
</header>
