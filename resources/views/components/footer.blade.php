<footer class="footer">
    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">
        Gustavo Felício
    </h5>

    <a href="https://github.com/gFelicio">
        GitHub
    </a>
</footer>
