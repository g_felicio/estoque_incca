<div class="form-wrapper">
    <form class="form-default"
        action="{{ route('orders.store') }}"
        method="POST">
        @csrf
        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Item
            </label>
            <input disabled type="text" name="item_name" value="{{ $item->name }}"
                class="form-default__item-wrapper__input">
            <input type="hidden" name="item_id" value="{{ $item->id }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Previsão de Chegada
            </label>
            <input type="date" name="arrival" required
                class="form-default__item-wrapper__input"
                placeholder="dd/mm/aaaa"
                value="{{ old('arrival') }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Quantidade do Item
            </label>
            <input type="number" name="quantity" step="1" required
                class="form-default__item-wrapper__input"
                value="{{ old('quantity') }}">
        </div>

        <div class="form-default__item-wrapper">
            <button type="submit" class="form-default__item-wrapper__button">
                Salvar Pedido
            </button>
        </div>
    </form>
</div>
