@extends('layouts.app')

@section('content')
    <div class="canvas-header space-between">
        Pedidos : : Fazer novo Pedido
    </div>

    @if (isset($order_pending) && $order_pending)
        <div class="message-alert">
            <div class="message-alert__action">
                <span class="message-alert__action--close" title="Fechar" id="message-alert-close">
                    &times;
                </span>
            </div>

            <div class="message-alert__message">
                O Item ainda tem um Pedido em aberto. Talvez você queira editar o Pedido existente, ao invés de abrir um novo.  <br>
                <a href="{{ route('orders.edit', $order_pending->id) }}"> Clique nesse link</a> para editar o Pedido em Aberto.
            </div>
        </div>
    @endif

    @include('pages.orders._create-form')
@endsection
