@extends('layouts.app')

@section('content')
    <div class="canvas-header space-between">
        Pedidos : : Editar Pedido {{ $order->id }} : : Item #{{ $item->id }} - {{ $item->name }}
    </div>

    @include('pages.orders._edit-form')
@endsection
