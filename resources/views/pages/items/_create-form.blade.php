<div class="form-wrapper">
    <form class="form-default"
        action="{{ route('items.store') }}"
        method="POST">
        @csrf
        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Nome do Item
            </label>
            <input type="text" name="name" required
                class="form-default__item-wrapper__input"
                value="{{ old('name') }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Descrição de Item
            </label>
            <input type="text" name="description" required
                class="form-default__item-wrapper__input"
                value="{{ old('description') }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Preço Unitário (em R$)
            </label>
            <input type="number" name="unit_price" step="0.01" required
                class="form-default__item-wrapper__input"
                value="{{ old('unit_price') }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Estoque de Item
            </label>
            <input type="number" name="stock_quantity" step="1" required
                class="form-default__item-wrapper__input"
                value="{{ old('stock_quantity') }}">
        </div>

        <div class="form-default__item-wrapper">
            <button type="submit" class="form-default__item-wrapper__button">
                Salvar Edições
            </button>
        </div>
    </form>
</div>
