@extends('layouts.app')

@section('content')
    <div class="canvas-header space-between">
        Estoque : : Listagem de Estoque

        <a href="{{ route('items.create') }}" title="Adicionar novo Item">
            <svg height="24" viewBox="0 0 24 24" width="24">
                <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm5 11h-4v4h-2v-4H7v-2h4V7h2v4h4v2z" fill="#FFF"/>
            </svg>

        </a>
    </div>

    <table class="table-default">
            <thead>
                <tr>
                    <th scope="col">
                        #ID
                    </th>
                    <th scope="col">
                        Nome
                    </th>
                    <th scope="col">
                        Descrição
                    </th>
                    <th scope="col">
                        Preço Unitário
                    </th>
                    <th scope="col">
                        Quantidade em Estoque
                    </th>
                    <th scope="col">
                        Estoque Mínimo
                    </th>
                    <th scope="col">
                        Fazer Pedido
                    </th>
                    <th scope="col">
                        Preço de Estoque por Item
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse($items as $item)
                    <tr>
                        <td scope="row">
                            {{ $item->id }}
                        </td>
                        <td>
                            <a class="table-link"
                                href="{{ route('items.edit', $item->id) }}" title="Editar Item">
                                {{ $item->name }}
                            </a>
                        </td>
                        <td>
                            <a class="table-link"
                                href="{{ route('items.edit', $item->id) }}" title="Editar Item">
                                {{ $item->description }}
                            </a>
                        </td>
                        <td>
                            R$ {{ number_format($item->unit_price, 2, ',', '.') }}
                        </td>
                        <td>
                            {{ $item->stock_quantity }}
                        </td>
                        <td>
                            {{ $item->minimum_stock }}
                        </td>
                        <td style="text-align: center">
                            @if ($item->stock_quantity < $item->minimum_stock)
                                    <a href="{{ route('orderItem', ['itemId' => $item->id]) }}" title="Fazer Pedido de Item">
                                        <svg height="24" viewBox="0 0 24 24" width="24">
                                            <path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-7 9h-2V5h2v6zm0 4h-2v-2h2v2z" fill="#e3a621"/>
                                        </svg>
                                    </a>
                            @else
                                <svg height="24" viewBox="0 0 24 24" width="24">
                                    <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-2z" fill="#18741a"/>
                                </svg>
                            @endif
                        </td>
                        <td>
                            R$ {{ number_format($item->stock_price, 2, ',', '.') }}
                        </td>
                    </tr>
                @empty
                    <tr class="empty">
                        <td colspan="8">
                            Nenhum Item Cadastrado
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {{ method_exists($items, 'links') ? $items->links() : '' }}
@endsection
