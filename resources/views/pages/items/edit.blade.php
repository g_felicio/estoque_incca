@extends('layouts.app')

@section('content')
    <div class="canvas-header">
        Estoque : : Editar Informações de Item
    </div>

    @include('pages.items._edit-form')
@endsection
