<div class="form-wrapper">
    <form class="form-default"
        action="{{ route('items.update', $item->id) }}"
        method="POST">
        @csrf
        @method('PUT')
        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Nome do Item
            </label>
            <input type="text" name="name"
                class="form-default__item-wrapper__input"
                value="{{ (isset($item) && $item) ? $item->name : '' }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Descrição de Item
            </label>
            <input type="text" name="description"
                class="form-default__item-wrapper__input"
                value="{{ (isset($item) && $item) ? $item->description : '' }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Preço Unitário (em R$)
            </label>
            <input type="number" name="unit_price" step="0.01"
                class="form-default__item-wrapper__input"
                value="{{ (isset($item) && $item) ? $item->unit_price : '' }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Estoque Mínimo de Item
            </label>
            <input type="number" name="minimum_stock"
                class="form-default__item-wrapper__input"
                value="{{ (isset($item) && $item) ? $item->minimum_stock : '' }}">
        </div>

        <div class="form-default__item-wrapper">
            <button type="submit" class="form-default__item-wrapper__button">
                Salvar Edições
            </button>
        </div>
    </form>
</div>
