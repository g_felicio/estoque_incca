@extends('layouts.app')

@section('content')
    <div class="canvas-header">
        Estoque : : Adicionar Novo Item
    </div>

    @include('pages.items._create-form')
@endsection
