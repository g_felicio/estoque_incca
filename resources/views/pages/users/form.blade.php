<div class="form-wrapper">
    <form class="form-default"
        action="{{ route('users.update', $user->id) }}"
        method="POST">
        @csrf
        @method('PUT')
        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                Nome de Usuário
            </label>
            <input type="text" name="name"
                class="form-default__item-wrapper__input"
                value="{{ (isset($user) && $user) ? $user->name : '' }}">
        </div>

        <div class="form-default__item-wrapper">
            <label class="form-default__item-wrapper__label">
                E-mail
            </label>
            <input type="email" name="email"
                class="form-default__item-wrapper__input"
                value="{{ (isset($user) && $user) ? $user->email : '' }}">
        </div>

        <div class="form-default__item-wrapper">
            <button type="submit" class="form-default__item-wrapper__button">
                Salvar Edições
            </button>
        </div>
    </form>
</div>
