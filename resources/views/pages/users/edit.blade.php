@extends('layouts.app')

@section('content')
    <div class="canvas-header">
        Editar dados : : {{ $user->name }}
    </div>

    @include('pages.users.form')
@endsection
