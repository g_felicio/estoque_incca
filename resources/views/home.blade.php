@extends('layouts.app')

@section('content')
    <div class="welcome-header">
        <h1>
            Seja bem-vindo, <span class="font-weight-bold">{{ Auth::user()->name }}</span>!
        </h1>

        <h3>
            Use o menu lateral para interagir com o sistema
        </h3>
    </div>
@endsection
