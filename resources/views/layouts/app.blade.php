<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            Estoque INCCA
        </title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        @include('components.header')

        <div class="main-content" style="padding-top: 0 !important; padding-bottom: 0 !important;">
            @if (Auth::check())
                @include('components.sidebar')
            @endif

            <div class="canvas">
                @if(session('message'))
                    <div class="message-alert">
                        <div class="message-alert__action">
                            <span class="message-alert__action--close" title="Fechar" id="message-alert-close">
                                &times;
                            </span>
                        </div>

                        <div class="message-alert__message">
                            {{ session('message') }}
                        </div>
                    </div>
                @endif

                @yield('content')
            </div>
        </div>
    </body>
</html>
