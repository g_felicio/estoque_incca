require('./bootstrap');

document.addEventListener("DOMContentLoaded", function() {
    if (document.getElementById('message-alert-close')) {
        let alertButton = document.getElementById('message-alert-close');

        alertButton.addEventListener('click', function() {
            let wrapper = alertButton.parentNode.parentNode;
            console.log(wrapper);

            if (!wrapper.classList.contains('el-hide')) {
                wrapper.classList.add('el-hide');
            }
        })
    }
});
