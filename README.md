# Estoque INCCA
Projeto desenvolvido para testes

## Como Rodar o Projeto
Para iniciar o projeto, é necessário clonar o repositório. Para isso, é preciso ter uma ferramenta de versionamento instalada. 

### Como instalar a Ferramenta de Versionamento git
Para instalar a ferramenta, [siga os seguintes passos](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git "Instruções para instalação do git em pt-BR").

### Como instalar NodeJs
Para instalar a ferramenta, [siga os seguintes passos](https://nodejs.org/en/download/package-manager/ "Instruções para instalação do NodeJS em inglês")

### Como instalar MySQL
Para instalar a ferramenta, [siga os seguintes passos](https://support.rackspace.com/how-to/install-mysql-server-on-the-ubuntu-operating-system/ "Instruções para instalação do MySQL em inglês").

### Como instalar Composer
Para instalar a ferramenta, [siga os seguintes passos, de acordo com o seu sistema operacional](https://getcomposer.org/download/ "Instruções para instalação do Composer em inglês")

### Já tenho git, NodeJS, MySQL e Composer instalados em minha máquina
Podemos passar para o passo de clonagem do repositório.

#### Clonagem do Repositório
Usando o terminal, entre em uma pasta de sua preferência.

##### Clonando usando HTTPS
Usando o terminal, escreva o seguinte:

`git clone https://g_felicio@bitbucket.org/g_felicio/estoque_incca.git`

A clonagem do repositório pode demorar algum tempo. Apenas espere o processo terminar.

##### Clonando usando SSH
Usando o terminal, escreva o seguinte:

`git clone git@bitbucket.org:g_felicio/estoque_incca.git`

A clonagem do repositório pode demorar algum tempo. Apenas espere o processo terminar.

#### Repositório Clonado!
Primeiramente devemos inicializar o `composer` e o `NodeJS`, que foram usados nesse projeto.

Rode, primeiramente, usando o terminal na raiz do projeto:

`composer install`

Esse comando pode demorar alguns minutos até ser finalizado.

Verifique se o comando anterior criou um arquivo `.env` na raiz de seu projeto.
Caso não tenha criado, duplique o arquivo `.env.example` e o nomeie apenas como `.env`.

Após duplicar o arquivo, rode o seguinte comando no terminal:

`php artisan key:generate`


Após rodar o comando anterior, rode:

`npm install`

Esse comando pode demorar alguns minutos até ser finalizado.

Após rodar esses dois comandos, o repositório está praticamente inicializado.


## Inicializando o banco de dados usando MySQL

Siga os seguintes passos para criar o banco de dados que vamos usar.

No terminal, digite os seguintes comandos, prestando atenção ao seu nome de usuário e senha do MySQL

1. mysql -u `nomeDeUsuario` -p
2. Digite a sua senha de usuário
3. grant all on `*`.`*` to 'nomeDeUsuario'@'%' identified by 'senhaDeUsuario';
4. flush privileges;
5. create database estoque_incca;
6. exit;
7. sudo service mysql restart

Após criar o banco de dados, no seu arquivo `.env`, edite os seguintes campos com as seguintes informações:

`DB_CONNECTION=mysql`

`DB_HOST=127.0.0.1`

`DB_PORT=3306` (Normalmente esta é a porta que o MySQL usa. Cheque essa informação e complete este campo com o número correto da porta.)

`DB_DATABASE=estoque_incca`

`DB_USERNAME=nomeDeUsuario`

`DB_PASSWORD=senhaDeUsuario`

Após editar e salvar o arquivo, rode o seguinte comando no terminal:

`php artisan migrate && php artisan db:seed`

Com isso, o banco de dados estará inicializado e já teremos algumas entradas para teste em algumas tabelas.

## Compilando assets
Para compilar os assets que são usados na aplicação, rode, a partir do terminal, o comando `npm run dev`. Todos os assets serão compilados e salvos em uma pasta acessível ao servidor.

## Inicializando o servidor para acesso ao sistema
Após seguir as etapas anteriores para inicializar a configuração de seu ambiente, devemos acessar o sistema em si, utilizando toda a sua interface. Para tal, apenas rode o comando

`php artisan serve`

através de seu terminal e a aplicação ficará disponível no endereço

`http://localhost:8000/`

## Registrando um usuário para utilizar o sistema

Agora que a aplicação está rodando em um browser de sua preferência, você deve criar um usuário para interagir com a aplicação.
Para isso, acesse o link `REGISTER` que se encontra no cabeçalho da página inicial.
Ao clicar no link, você será redirecionado para uma página onde deve preencher alguns campos obrigatórios para o seu login.
Após cadastrar-se, parabéns. Você já pode acessar as funcionalidades do sistema.
